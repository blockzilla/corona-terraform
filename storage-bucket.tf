resource "google_storage_bucket" "corona-data" {
  name     = "corona-data-bucket"
  location = "EU"

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}
