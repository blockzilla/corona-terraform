provider "google" {
  version     = "3.0.0"
  credentials = "./creds/serviceaccount.json"
  project     = var.project_id
  region      = var.region
}
