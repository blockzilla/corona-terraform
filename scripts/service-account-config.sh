gcloud auth login

gcloud init

#!/bin/sh
gcloud services list --available

gcloud services enable compute.googleapis.com
gcloud services enable servicenetworking.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable container.googleapis.com
gcloud services enable dataproc.googleapis.com
gcloud services enable compute-component.googleapis.com
gcloud services enable geolocation.googleapis.com
gcloud services enable geocoding-backend.googleapis.com


# Create service account
gcloud iam service-accounts create terraform-gke


gcloud projects add-iam-policy-binding coronavirus-271700 --member serviceAccount:terraform-gke@coronavirus-271700.iam.gserviceaccount.com --role roles/dataproc.admin


# Set service account roles
gcloud projects add-iam-policy-binding blockzilla --member serviceAccount:terraform-gke@blockzilla.iam.gserviceaccount.com --role roles/dataproc.admin
gcloud projects add-iam-policy-binding blockzilla --member serviceAccount:terraform-gke@blockzilla.iam.gserviceaccount.com --role roles/container.admin
gcloud projects add-iam-policy-binding blockzilla --member serviceAccount:terraform-gke@blockzilla.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding blockzilla --member serviceAccount:terraform-gke@blockzilla.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
gcloud projects add-iam-policy-binding blockzilla --member serviceAccount:terraform-gke@blockzilla.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin


# Create credential file
gcloud iam service-accounts keys create terraform-gke-keyfile.json --iam-account=terraform-gke@blockzilla.iam.gserviceaccount.com


# Create terraform storage bucket
gsutil mb -p blockzilla -c regional -l europe-west4 gs://terraform-state-bucket-gke/
gsutil versioning set on gs://terraform-state-bucket-gke/
gsutil iam ch serviceAccount:terraform-gke@blockzilla.iam.gserviceaccount.com:legacyBucketWriter gs://terraform-state-bucket-gke/
