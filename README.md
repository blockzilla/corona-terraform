# CloudSystems-Assignment1

Installation details




# Install Tools


*  [terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)
*  [gcloud](https://cloud.google.com/sdk/docs/)
*  [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
*  [multiple gcloud configs](https://stackoverflow.com/questions/44820119/how-to-use-multiple-service-accounts-with-gcloud)



# gCloud config
Execute the following script to create service account and roles
[`./scripts/service-account-config.sh`](./scripts/service-account-config.sh)


# Connect GitLab CI to k8s
https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster




# Terraform project
https://timberry.dev/posts/terraform-pipelines-in-gitlab/
https://medium.com/@timhberry/learn-terraform-by-deploying-a-google-kubernetes-engine-cluster-a29071d9a6c2
https://www.padok.fr/en/blog/kubernetes-google-cloud-terraform-cluster

