terraform {
  backend "gcs" {
    credentials = "./creds/serviceaccount.json"
    bucket      = "terraform-state-bucket-gke"
    prefix      = "terraform/state"
  }
}

